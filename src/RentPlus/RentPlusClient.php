<?php

namespace TakeTheLead\RentPlus;

use Psr\Log\LoggerInterface;
use SoapClient;

class RentPlusClient
{
    private $wsdlUrl;
    private $proxyHost;
    private $proxyPort;
    private $client;
    private $logger;

    /**
     * RentPlusClient constructor.
     * @param $wsdlUrl
     * @param $proxyHost
     * @param $proxyPort
     * @param LoggerInterface $logger
     * @throws \SoapFault
     */
    public function __construct(
        $wsdlUrl,
        $proxyHost,
        $proxyPort,
        LoggerInterface $logger
    ) {
        $this->wsdlUrl = $wsdlUrl;
        $this->proxyHost = $proxyHost;
        $this->proxyPort = $proxyPort;
        $this->logger = $logger;

        $context = stream_context_create([
            'ssl' => [
                'verify_peer' => true,
                'verify_peer_name' => true,
                'allow_self_signed' => true,
            ]
        ]);

        $this->client = new SoapClient($this->wsdlUrl, [
            'proxy_host' => ($this->proxyHost) ? $this->proxyHost : null,
            'proxy_port' => ($this->proxyPort) ? $this->proxyPort : null,
            'trace' => 1,
            'exception' => 0,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'verifypeer' => true,
            'verifyhost' => true,
            'stream_context' => $context,
        ]);
    }

    /**
     * @param $house
     * @param array $args
     * @return bool|\SimpleXMLElement
     */
    public function getCurrentOrders($house, $args = [])
    {
        $args = array_merge($this->getCurrentOrdersDefaultArgs(), $args);
        $args['wareHouseNumber'] = $house;

        try {
            return $this->callApi('GetCurrentOrders',
                $args,
                [
                    'argument' => 'warehouse',
                    'value' => $house
                ]
            );
        } catch (\Exception $e) {
            $this->logger->critical('Cannot receive orders from rent+ of warehouse {house}', ['house' => $house]);
        }

        return false;
    }

    public function getCurrentOrdersWithLastModifDate($house, \DateTime $lastModifDate, $args = [])
    {
        $args = array_merge($this->getCurrentOrdersDefaultArgs(), $args);
        $args['wareHouseNumber'] = $house;
        $args['lastModifDate'] = $lastModifDate->format('Y-m-d H:i:s');

        try {
            return $this->callApi('GetCurrentOrdersWithLastModifDate',
                $args,
                [
                    'argument' => 'warehouse',
                    'value' => $house
                ]
            );
        } catch (\Exception $e) {
            $this->logger->critical('Cannot receive orders from rent+ of warehouse {house}', ['house' => $house]);
        }

        return false;
    }

    /**
     * @return array
     */
    protected function getCurrentOrdersDefaultArgs(): array
    {
        return [
            'dateToConsider' => 3,
            'startDate' => date('Y-m-d', strtotime('-30 days')),
            'endDate' => date('Y-m-d', strtotime('+30 days')),
            'showOffer' => true,
            'showActiveOption' => true,
            'showTimeOutOption' => true,
            'showOrderNotToPrepare' => true,
            'showOrderToPrepare' => true,
            'showOrderPrepareListPrinted' => true,
            'showOrderScanBegan' => false,
            'showOrderScanEnded' => false,
            'showOrderPrepareManuallyEnded' => true,
            'showDelivered' => true,
            'showDeliveredLate' => false,
            'showReturnParked' => false,
            'showCancel' => true,
            'showReturnDone' => false,
        ];
    }

    /**
     * @param $orderNr
     * @return bool|\SimpleXMLElement
     */
    public function getOrderDetails($orderNr)
    {
        try {
            return $this->callApi('GetOrderDetail',
                [
                    'orderNumber' => $orderNr,
                ],
                [
                    'argument' => 'order number',
                    'value' => $orderNr
                ]
            );
        } catch (\Exception $e) {
            $this->logger->critical('Cannot receive order details from rent+ of order nr: {order}', ['order' => $orderNr]);
        }

        return false;
    }

    /**
     * @param $articleKey
     * @return bool|\SimpleXMLElement
     */
    public function getArticleInfo($articleKey)
    {
        try {
            return $this->callApi('GetArticleInfo',
                [
                    'articleKey' => $articleKey,
                ],
                [
                    'argument' => 'article key',
                    'value' => $articleKey
                ]);
        } catch (\Exception $e) {
            $this->logger->critical('Cannot receive article details from rent+ of article key: {article}', ['article' => $articleKey]);
        }

        return false;
    }

    /**
     * @param $customerKey
     * @return bool|\SimpleXMLElement
     */
    public function getOrderContactInfo($customerKey)
    {
        try {
            return $this->callApi('GetContactInfo',
                [
                    'customerKey' => $customerKey,
                ],
                [
                    'argument' => 'customer key',
                    'value' => $customerKey
                ]
            );
        } catch (\Exception $e) {
            $this->logger->critical('Cannot receive order contact details from rent+ of customer key: {customerKey}', ['customerKey' => $customerKey]);
        }

        return false;
    }

    /**
     * @param \DateTime|null $from
     * @return bool|\SimpleXMLElement
     */
    public function getCustomerList(?\DateTime $from = null)
    {
        try {
            return $this->callApi('GetCustomerList',
                [
                    'from' => $from ? $from->format('Y-m-d') : null,
                ],
                [
                    'argument' => 'from',
                    'value' => $from ? $from->format('Y-m-d') : null,
                ]
            );
        } catch (\Exception $e) {
            $this->logger->critical('Cannot receive customer list from rent+');
        }

        return false;
    }

    /**
     * @param string $customerKey
     * @param string $warehouseNumber
     * @return bool|\SimpleXMLElement
     */
    public function getCustomerInfoByKey($customerKey, $warehouseNumber = '')
    {
        try {
            return $this->callApi('GetCustomerInfo',
                [
                    'customerKey' => $customerKey,
                    'Venture_Number' => '',
                    'WarehouseNumber' => $warehouseNumber,
                ],
                [
                    'argument' => 'customer key',
                    'value' => $customerKey
                ]
            );
        } catch (\Exception $e) {
            $this->logger->critical('Cannot receive order contact details from rent+ of customer key: {customerKey}', ['customerKey' => $customerKey]);
        }

        return false;
    }

    /**
     * @param $customerKey
     * @param $warehouseNumber
     * @param $numberInAccountancy
     */
    public function setCustomerNumberInAccountancy($customerKey, $warehouseNumber, $numberInAccountancy)
    {
        try {
            return $this->callApi('SetCustomerNumberInAccountancy',
                [
                    'customerKey' => $customerKey,
                    'warehouseNr' => $warehouseNumber,
                    'numberInAccountancy' => $numberInAccountancy,
                ],
                [
                    'argument' => 'customer key',
                    'value' => $customerKey
                ]
            );
        } catch (\Exception $e) {
            $this->logger->critical('Cannot setCustomerNumberInAccountancy for customer key: {customerKey}', ['customerKey' => $customerKey]);
        }

        return false;
    }

    /**
     * @param $method
     * @param array $args
     * @param array $info
     * @return bool|\SimpleXMLElement
     */
    private function callApi($method, $args = [], $info = [])
    {
        $result = $this->client->__soapCall($method, [$method => $args]);

        $xml = str_replace(array("diffgr:", "msdata:"), '', $result->{$method . 'Result'}->any);
        $xml = str_replace('www.rentplus', 'https://rentplus', $xml);
        $xml = "<package>" . $xml . "</package>";
        $data = simplexml_load_string($xml);

        return $data;
    }
}
