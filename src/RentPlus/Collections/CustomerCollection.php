<?php

namespace TakeTheLead\RentPlus\Collections;

use Illuminate\Support\Collection;
use TakeTheLead\RentPlus\DTO\Customer;

class CustomerCollection extends Collection
{
    /**
     * @return CustomerCollection
     */
    public function withoutNumberInAccountancy()
    {
        return $this->filter(function (Customer $customer) {
            return !$customer->hasNumberInAccountancy();
        });
    }

    /**
     * @return CustomerCollection
     */
    public function withVentureNumber()
    {
        return $this->filter(function (Customer $customer) {
            return $customer->hasVentureNumber();
        });
    }

    /**
     * @return CustomerCollection
     */
    public function withoutVentureNumber()
    {
        return $this->filter(function (Customer $customer) {
            return !$customer->hasVentureNumber();
        });
    }
}
