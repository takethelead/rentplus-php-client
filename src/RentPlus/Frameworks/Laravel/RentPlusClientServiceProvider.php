<?php

namespace TakeTheLead\RentPlus\Frameworks\Laravel;

use Illuminate\Log\Logger;
use Illuminate\Support\ServiceProvider;
use TakeTheLead\RentPlus\RentPlusClient;

class RentPlusClientServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/config.php' => config_path('rentplus.php'),
            ], 'config');
        }

        $this->app->bind(RentPlusClient::class, function ($app) {
            return new RentPlusClient(
                config('rentplus.wsdl_url'),
                config('rentplus.proxy_host'),
                config('rentplus.proxy_port'),
                app(Logger::class)
            );
        });
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/config.php', 'rentplus');
    }
}
