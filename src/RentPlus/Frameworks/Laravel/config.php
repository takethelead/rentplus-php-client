<?php

return [
    
    'wsdl_url' => env('RENTPLUS_WSDL_URL'),
    'proxy_host' => env('RENTPLUS_PROXY_HOST'),
    'proxy_port' => env('RENTPLUS_PROXY_PORT'),

];
