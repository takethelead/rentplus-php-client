<?php

namespace TakeTheLead\RentPlus\DTO;

use ReflectionClass;
use ReflectionProperty;
use SimpleXMLElement;

class DataTransferObject
{
    public static function fromXml(SimpleXMLElement $xml)
    {
        $object = new static();

        $class = new ReflectionClass(static::class);

        foreach ($class->getProperties(ReflectionProperty::IS_PUBLIC) as $reflectionProperty) {
            $propertyName = $reflectionProperty->getName();
            $object->$propertyName = (string)$xml->$propertyName;
        }

        return $object;
    }
}
