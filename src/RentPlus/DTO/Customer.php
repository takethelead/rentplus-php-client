<?php

namespace TakeTheLead\RentPlus\DTO;

use Illuminate\Support\Str;
use SimpleXMLElement;

class Customer extends DataTransferObject
{
    public $Customer_Key;
    public $Name_1;
    public $Name_2;
    public $Address_Line_1;
    public $Address_Line_2;
    public $Postal_Code;
    public $Sub_Postal_Code;
    public $City;
    public $Country;
    public $Establishment_Place;
    public $Telephone;
    public $Fax;
    public $VAT_Prefix;
    public $Venture_Number;
    public $Rental_Discount_Percentage;
    public $Sales_Discount_Percentage;
    public $Mobile_Phone;
    public $Email;
    public $Contact_Person;
    public $Category_1_Code;
    public $Category_1_Description;
    public $Price_Category_Code;
    public $Price_Category_Description;
    public $Rental_Allowed;
    public $Language;
    public $Number_In_Accountancy;
    public $Payment_Term;
    public $Date_Creation;
    public $Date_LastModification;
    public $Warehouse;

    public static function fromXmlWithWarehouse(SimpleXMLElement $xml, string $warehouse)
    {
        $object = static::fromXml($xml);

        $object->Warehouse = $warehouse;

        return $object;
    }

    public function hasNumberInAccountancy()
    {
        return !empty($this->Number_In_Accountancy);
    }

    public function hasVentureNumber()
    {
        return !empty($this->Venture_Number);
    }

    public function getStandardizedVatNumber()
    {
        if (empty($this->Venture_Number)) {
            return null;
        }

        $ventureNumber = str_replace(['.', ' '], '', $this->Venture_Number);

        if ($this->VAT_Prefix === 'BE' && strlen($ventureNumber) === 9) {
            $ventureNumber = '0' . $ventureNumber;
        }

        return $this->VAT_Prefix . $ventureNumber;
    }
}
