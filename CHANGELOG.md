# Changelog

All notable changes to `rentplus-php-client` will be documented in this file

## 1.0.0 - 2019-11-05

- initial release
